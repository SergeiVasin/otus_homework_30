﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_30
{
    [Serializable]
    public class Animal : Organism //, IMyCloneable<Animal>
    {
        public Animal() 
        {
            objectToClone = this;
        }
        public BreedingType BreedingType { get; set; }
        public string? Hubitat { get; set; }
        public AnimalType AnimalType { get; set; }

        public Head? Head { get; set; }

        public override Animal MyClone()
        {
            return (Animal)base.MyClone();
        }

        public override string ToString()
        {
            return $"\r\n\tId: {Id};\r\n\tName: {Name};\r\n\tCellularType: {CellularType};\r\n" +
                $"\tBreedingType: {BreedingType};\r\n\tHubitat: {Hubitat};\r\n\tAnimalType: {AnimalType};\r\n\t" +
                $"HeadForm: {Head.Form};\r\n\tHeadColor: {Head.Color};\r\n";
        }
    }
}
