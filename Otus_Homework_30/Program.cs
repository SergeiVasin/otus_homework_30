﻿

using System.Text.Json;

namespace Otus_Homework_30
{
    class Program
    {
        static void Main()
        {
            CheckOrganism();

            CheckAnimal();

            CheckInsect();

            //Создать тесты на : не такой же объект; не такие же свойства при их изменении,
            // глубокое клонирование

        }

        static void CheckOrganism() 
        {
            var organism = new Organism
            {
                Id = 1,
                Name = "Worm",
                CellularType = CellularType.Multicellular
            };

            var clonedOrganism = (Organism)organism.Clone();

            var myClonedOrganism = organism.MyClone();

            Console.WriteLine($"Is clonedOrganism the same as original? {organism == clonedOrganism}.");
            Console.WriteLine($"Is myClonedOrganism the same as original? {organism == myClonedOrganism}.");

            Console.WriteLine();
            Console.WriteLine($"Organisms before overwriting:");
            Console.WriteLine($" myClonedOrganism: {myClonedOrganism}");
            Console.WriteLine($" clonedOrganism: {myClonedOrganism}");
            Console.WriteLine($" organism: {organism}");

            clonedOrganism.Id = 2;
            clonedOrganism.Name = "Infusoria";
            clonedOrganism.CellularType = CellularType.Unicellular;

            myClonedOrganism.Id = 3;
            myClonedOrganism.Name = "Cockroach";

            Console.WriteLine();
            Console.WriteLine($"Organisms after overwriting:");
            Console.WriteLine($" myClonedOrganism: {myClonedOrganism}");
            Console.WriteLine($" clonedOrganism: {clonedOrganism}");
            Console.WriteLine($" organism: {organism}");

            Console.WriteLine($"----------------------------------------------------------------------------");
            Console.WriteLine();

        }

        static void CheckAnimal()
        {
            var animal = new Animal
            {
                Id = 1,
                Name = "Сat",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Mammal,
                Hubitat = "Urban habitat",
                BreedingType = BreedingType.Viviparous,
                Head = new Head { Form = "Square", Color = "Red" }                
            };
            
            var clonedAnimal = (Animal)animal.Clone();

            var myClonedAnimal = animal.MyClone();

            Console.WriteLine($"Is clonedAnimal the same as original? {animal == clonedAnimal}.");
            Console.WriteLine($"Is myClonedAnimal the same as original? {animal == myClonedAnimal}.");

            Console.WriteLine();
            Console.WriteLine($"Animals before overwriting:");
            Console.WriteLine($" myClonedAnimal: {myClonedAnimal}");
            Console.WriteLine($" clonedAnimal: {clonedAnimal}");
            Console.WriteLine($" animal: {animal}");

            clonedAnimal.Id = 2;
            clonedAnimal.Name = "Wolf";
            clonedAnimal.Hubitat = "Wood";
            clonedAnimal.Head.Color = "Green";
            clonedAnimal.Head.Form = "Triangular";

            myClonedAnimal.Id = 3;
            myClonedAnimal.Name = "Pantera";
            myClonedAnimal.Hubitat = "Jungle";
            myClonedAnimal.Head.Color = "Black";
            myClonedAnimal.Head.Form = "Hexagonal";

            Console.WriteLine();
            Console.WriteLine($"Animals after overwriting:");
            Console.WriteLine($" myClonedAnimal: {myClonedAnimal}");
            Console.WriteLine($" clonedAnimal: {clonedAnimal}");
            Console.WriteLine($" animal: {animal}");

            Console.WriteLine($"----------------------------------------------------------------------------");
            Console.WriteLine();
        }

        static void CheckInsect()
        {
            var insect = new Insect
            {
                Id = 1,
                Name = "Dor",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Desert",
                BreedingType = BreedingType.EggLaying,
                Head = new Head { Form = "Square", Color = "Black" },
                InsectType = "Coleoptera"
            };

            var clonedInsect = (Insect)insect.Clone();

            var myClonedInsect = insect.MyClone();

            Console.WriteLine($"Is clonedInsect the same as original? {insect == clonedInsect}.");
            Console.WriteLine($"Is myClonedInsect the same as original? {insect == myClonedInsect}.");

            Console.WriteLine();
            Console.WriteLine($"Insects before overwriting:");
            Console.WriteLine($" MyClonedInsect: {myClonedInsect}");
            Console.WriteLine($" ClonedInsect: {clonedInsect}");
            Console.WriteLine($" Insect: {insect}");

            clonedInsect.Id = 2;
            clonedInsect.Name = "Phobaeticus kirbyi";
            clonedInsect.Hubitat = "Wood";
            clonedInsect.Head.Color = "Green";
            clonedInsect.Head.Form = "Triangular";
            clonedInsect.InsectType = "Stick insect";

            myClonedInsect.Id = 3;
            myClonedInsect.Name = "Attacus atlas";
            myClonedInsect.Hubitat = "Jungle";
            myClonedInsect.Head.Color = "Black";
            myClonedInsect.Head.Form = "Hexagonal";
            myClonedInsect.InsectType = "Butterfly";

            Console.WriteLine();
            Console.WriteLine($"Insects after overwriting:");
            Console.WriteLine($" MyClonedInsect: {myClonedInsect}");
            Console.WriteLine($" ClonedInsect: {clonedInsect}");
            Console.WriteLine($" Insect: {insect}");

            Console.WriteLine($"---------------------------------------------------------------------------");
            Console.WriteLine("-----The End------");

        }
    }
}