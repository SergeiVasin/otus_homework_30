﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_30
{
    [Serializable]
    public class Organism: IMyCloneable<Organism>, ICloneable
    {
        protected object? objectToClone;
        public Organism()
        {
            objectToClone = this;
        }

        protected Organism(object obj)
        {
            objectToClone = obj;
        }
        public int Id { get; set; }
        public string? Name { get; set; }
        public CellularType CellularType { get; set; }

        public object Clone()
        {
            using var tempStream = new MemoryStream();
            
            var binFormatter = new BinaryFormatter(null,
                new StreamingContext(StreamingContextStates.Clone));

            binFormatter.Serialize(tempStream, objectToClone);
            tempStream.Seek(0, SeekOrigin.Begin);

            objectToClone = binFormatter.Deserialize(tempStream);
            
            return objectToClone;
        }
         
        public virtual Organism MyClone()
        {
            using var tempStream = new MemoryStream();

            var binFormatter = new BinaryFormatter(null,
                new StreamingContext(StreamingContextStates.Clone));

            binFormatter.Serialize(tempStream, objectToClone);
            tempStream.Seek(0, SeekOrigin.Begin);

            objectToClone = binFormatter.Deserialize(tempStream);

            return (Organism)objectToClone;
        }

        public override string ToString()
        {
            return $"\r\n\tId: {Id};\r\n\tName: {Name};\r\n\tCellularType: {CellularType}";
        }
    }
}
