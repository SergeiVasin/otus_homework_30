﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_30
{
    [Serializable]
    public class Insect : Animal
    {
        public Insect()
        {
            objectToClone = this;
        }
        public string? InsectType { get; set; }

        public override Insect MyClone()
        {
            return (Insect)base.MyClone();
        }

        public override string ToString()
        {
            return $"\r\n\tId: {Id};\r\n\tName: {Name};\r\n\tCellularType: {CellularType};\r\n\t" +
                $"BreedingType: {BreedingType};\r\n\tHubitat: {Hubitat};\r\n\tAnimalType: {AnimalType};\r\n\t" +
                $"InsectType: {InsectType};\r\n\tHeadForm: {Head.Form};\r\n\tHeadColor: {Head.Color};\r\n\t";
        }
    }
}
