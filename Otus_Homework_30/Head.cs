﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_30
{
    [Serializable]
    public class Head
    {
        public string? Form { get; set; }
        public string? Color { get; set; }
    }
}
