﻿using Otus_Homework_30;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class AnimalTests
    {
        [Fact]
        public void CheckObjectNotSameWhenUseClone()
        {
            var animal = new Animal();

            var result = animal.Clone() as Animal;

            Assert.NotSame(animal, result);
        }

        [Fact]
        public void CheckObjectNotSameWhenUseMyClone()
        {
            var animal = new Animal();

            var result = animal.MyClone();

            Assert.NotSame(animal, result);
        }

        [Fact]
        public void CheckObjectNotNullWhenUseClone()
        {
            var animal = new Animal();

            var result = animal.Clone() as Animal;

            Assert.NotNull(result);
        }

        [Fact]
        public void CheckObjectNotNullWhenUseMyClone()
        {
            var animal = new Animal();

            var result = animal.MyClone();

            Assert.NotNull(result);
        }

        [Fact]
        public void CheckIsObjectAnAnimalTypeWhenUseClone()
        {
            var animal = new Animal();

            var result = animal.Clone();

            Assert.IsType<Animal>(result);
        }

        [Fact]
        public void CheckIsObjectAnAnimalTypeWhenUseMyClone()
        {
            var animal = new Animal();

            var result = animal.MyClone();

            Assert.IsType<Animal>(result);
        }

        [Fact]
        public void CheckPropertiesNotSameWhenChangingProperiesAfterClone()
        {
            //
            var animal = new Animal
            {
                Id = 1,
                Name = "Tiger",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Jungle",
                BreedingType = BreedingType.Viviparous,
                Head = new Head { Form = "Oval", Color = "Orange" }
            };
            //
            var result = animal.Clone() as Animal;
            result.Id = 2;
            result.Name = "Cockroach";
            result.CellularType = CellularType.Multicellular;
            result.AnimalType = AnimalType.Insect;
            result.Hubitat = "Urban habitat";
            result.BreedingType = BreedingType.EggLaying;
            result.Head = new Head { Form = "Square", Color = "Brown" };

            var serializedAnimal = JsonSerializer.Serialize(animal);
            var serializedResult = JsonSerializer.Serialize(result); 

            //
            Assert.NotEqual(serializedAnimal, serializedResult);
        }

        [Fact]
        public void CheckPropertiesNotSameWhenChangingProperiesAfterMyClone()
        {
            //
            var animal = new Animal
            {
                Id = 1,
                Name = "Tiger",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Jungle",
                BreedingType = BreedingType.Viviparous,
                Head = new Head { Form = "Oval", Color = "Orange" }
            };
            //
            var result = animal.MyClone();
            result.Id = 2;
            result.Name = "Cockroach";
            result.CellularType = CellularType.Multicellular;
            result.AnimalType = AnimalType.Insect;
            result.Hubitat = "Urban habitat";
            result.BreedingType = BreedingType.EggLaying;
            result.Head = new Head { Form = "Square", Color = "Brown" };

            var serializedAnimal = JsonSerializer.Serialize(animal);
            var serializedResult = JsonSerializer.Serialize(result);

            //
            Assert.NotEqual(serializedAnimal, serializedResult);
        }

        [Fact]
        public void CheckIsAllPropertiesClonedWhenClone()
        {
            var animal = new Animal
            {
                Id = 1,
                Name = "Tiger",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Jungle",
                BreedingType = BreedingType.Viviparous,
                Head = new Head { Form = "Oval", Color = "Orange" }
            };
            //
            var result = animal.Clone() as Animal;

            //
            var serializedHeadAnimal = JsonSerializer.Serialize(animal.Head);
            var serializedHeadResult = JsonSerializer.Serialize(result?.Head);

            Assert.Equal(serializedHeadAnimal, serializedHeadResult);
        }

        [Fact]
        public void CheckIsAllPropertiesClonedWhenMyClone()
        {
            var animal = new Animal
            {
                Id = 1,
                Name = "Tiger",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Jungle",
                BreedingType = BreedingType.Viviparous,
                Head = new Head { Form = "Oval", Color = "Orange" }
            };
            //
            var result = animal.MyClone();

            //
            var serializedHeadAnimal = JsonSerializer.Serialize(animal.Head);
            var serializedHeadResult = JsonSerializer.Serialize(result?.Head);

            Assert.Equal(serializedHeadAnimal, serializedHeadResult);
        }
    }
}
