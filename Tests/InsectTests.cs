﻿using Otus_Homework_30;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class InsectTests
    {
        [Fact]
        public void CheckObjectNotSameWhenUseClone()
        {
            var insect = new Insect();

            var result = insect.Clone() as Insect;

            Assert.NotSame(insect, result);
        }

        [Fact]
        public void CheckObjectNotSameWhenUseMyClone()
        {
            var insect = new Insect();

            var result = insect.MyClone();

            Assert.NotSame(insect, result);
        }

        [Fact]
        public void CheckObjectNotNullWhenUseClone()
        {
            var insect = new Insect();

            var result = insect.Clone() as Insect;

            Assert.NotNull(result);
        }

        [Fact]
        public void CheckObjectNotNullWhenUseMyClone()
        {
            var insect = new Insect();

            var result = insect.MyClone();

            Assert.NotNull(result);
        }

        [Fact]
        public void CheckIsObjectAnInsectTypeWhenUseClone()
        {
            var insect = new Insect();

            var result = insect.Clone();

            Assert.IsType<Insect>(result);
        }

        [Fact]
        public void CheckIsObjectAnInsectTypeWhenUseMyClone()
        {
            var insect = new Insect();

            var result = insect.MyClone();

            Assert.IsType<Insect>(result);
        }

        [Fact]
        public void CheckPropertiesNotSameWhenChangingProperiesAfterClone()
        {
            //
            var insect = new Insect
            {
                Id = 1,
                Name = "Dor",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Desert",
                BreedingType = BreedingType.EggLaying,
                Head = new Head { Form = "Square", Color = "Black" },
                InsectType = "Coleoptera"
            };
            //
            var result = insect.Clone() as Insect;
            result.Id = 2;
            result.Name = "Attacus atlas";
            result.CellularType = CellularType.Multicellular;
            result.AnimalType = AnimalType.Insect;
            result.Hubitat = "Wood";
            result.BreedingType = BreedingType.EggLaying;
            result.Head = new Head { Form = "Triangle", Color = "Brown" };
            result.InsectType = "Butterfly";

            var serializedInsect = JsonSerializer.Serialize(insect);
            var serializedResult = JsonSerializer.Serialize(result);

            //
            Assert.NotEqual(serializedInsect, serializedResult);
        }

        [Fact]
        public void CheckPropertiesNotSameWhenChangingProperiesAfterMyClone()
        {
            //
            var insect = new Insect
            {
                Id = 1,
                Name = "Dor",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Desert",
                BreedingType = BreedingType.EggLaying,
                Head = new Head { Form = "Square", Color = "Black" },
                InsectType = "Coleoptera"
            };
            //
            var result = insect.MyClone();
            result.Id = 2;
            result.Name = "Attacus atlas";
            result.CellularType = CellularType.Multicellular;
            result.AnimalType = AnimalType.Insect;
            result.Hubitat = "Wood";
            result.BreedingType = BreedingType.EggLaying;
            result.Head = new Head { Form = "Triangle", Color = "Brown" };
            result.InsectType = "Butterfly";

            var serializedInsect = JsonSerializer.Serialize(insect);
            var serializedResult = JsonSerializer.Serialize(result);

            //
            Assert.NotEqual(serializedInsect, serializedResult);
        }

        [Fact]
        public void CheckIsAllPropertiesClonedWhenClone()
        {
            var insect = new Insect
            {
                Id = 1,
                Name = "Dor",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Desert",
                BreedingType = BreedingType.EggLaying,
                Head = new Head { Form = "Square", Color = "Black" },
                InsectType = "Coleoptera"
            };
            //
            var result = insect.Clone() as Insect;

            //
            var serializedHeadInsect = JsonSerializer.Serialize(insect.Head);
            var serializedHeadResult = JsonSerializer.Serialize(result?.Head);

            Assert.Equal(serializedHeadInsect, serializedHeadResult);
        }

        [Fact]
        public void CheckIsAllPropertiesClonedWhenMyClone()
        {
            var insect = new Insect
            {
                Id = 1,
                Name = "Dor",
                CellularType = CellularType.Multicellular,
                AnimalType = AnimalType.Insect,
                Hubitat = "Desert",
                BreedingType = BreedingType.EggLaying,
                Head = new Head { Form = "Square", Color = "Black" },
                InsectType = "Coleoptera"
            };
            //
            var result = insect.MyClone();

            //
            var serializedHeadInsect = JsonSerializer.Serialize(insect.Head);
            var serializedHeadResult = JsonSerializer.Serialize(result?.Head);

            Assert.Equal(serializedHeadInsect, serializedHeadResult);
        }
    }
}
