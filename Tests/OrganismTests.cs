using Otus_Homework_30;
using Xunit;

namespace Tests
{
    public class OrganismTests
    {
        [Fact]
        public void CheckObjectNotSameWhenUseClone()
        {
            var organism = new Organism();

            var result = organism.Clone() as Organism;

            Assert.NotSame(organism, result);
        }

        [Fact]
        public void CheckObjectNotSameWhenUseMyClone()
        {
            var organism = new Organism();

            var result = organism.MyClone();

            Assert.NotSame(organism, result);
        }

        [Fact]
        public void CheckObjectNotNullWhenUseClone()
        {
            var organism = new Organism();

            var result = organism.Clone() as Organism;

            Assert.NotNull(result);
        }

        [Fact]
        public void CheckObjectNotNullWhenUseMyClone()
        {
            var organism = new Organism();

            var result = organism.MyClone();

            Assert.NotNull(result);
        }

        [Fact]
        public void CheckIsObjectAnOrganismTypeWhenUseClone()
        {
            var organism = new Organism();

            var result = organism.Clone();

            Assert.IsType<Organism>(result);
        }

        [Fact]
        public void CheckIsObjectAnOrganismTypeWhenUseMyClone()
        {
            var organism = new Organism();

            var result = organism.MyClone();

            Assert.IsType<Organism>(result);
        }
    }
}